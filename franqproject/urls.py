from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('pessoa/', include('Pessoa.urls')),
    path('garagem/', include('Garagem.urls'))
]

TIPO_PESSOA = ((0, "Consumidor"), (1, "Administrador"))
CATEGORIA_VEICULO = ((1,"Bicicleta"),(2,"Ciclomotor"),(3,"Motoneta"),(4,"Motocicleta"),(5,"Triciclo"),(6,"Quadriciclo"),(7,"Automóvel") \
    ,(8,"Microônibus"),(9,"Ônibus"),(10,"Bonde"),(11,"Reboque ou Semi-Reboque"),(12,"Charrete"))
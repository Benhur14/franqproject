from django.urls import path
from .views import *

app_name = 'pessoa'

urlpatterns = [
    path('list/', pessoaListView),
    path('insert/', pessoaView),
    path('insert/<int:id>/', pessoaView)
]

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from Garagem.models import *

@api_view(['GET'])
def pessoaListView(request):
    pessoas = Pessoa.objects.all().values()       
    return Response(pessoas)

@api_view(['POST', 'DELETE', 'PUT'])
def pessoaView(request, id=None):
    if request.method == 'POST' or request.method == 'PUT':
        pessoa = Pessoa(**request.data)
        pessoa.save()

        garagem = Garagem()
        garagem.id_pessoa = Pessoa.objects.get(pk=pessoa.pk)
        garagem.save()

        return Response(pessoa.pk)

    elif request.method == 'DELETE':
        pessoa = Pessoa.objects.get(pk=id)
        pessoa.delete()
        return Response(status=200)
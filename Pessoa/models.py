from franqproject.choices import TIPO_PESSOA
from django.db import models

class Pessoa(models.Model):
    nome = models.CharField(max_length=100, null=False, blank=False)
    telefone = models.CharField(max_length=40, null=False, blank=False)
    email = models.CharField(max_length=40, null=False, blank=False)
    tipo = models.IntegerField(choices=TIPO_PESSOA)

    class Meta:
        db_table = 'pessoa'

    def __str__(self):
        return self.nome


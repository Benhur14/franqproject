from franqproject.choices import CATEGORIA_VEICULO, TIPO_PESSOA
from django.db import models

class Veiculo(models.Model):
    placa = models.CharField(max_length=10, null=False, blank=False)
    modelo = models.CharField(max_length=100, null=False, blank=False)
    categoria = models.IntegerField(choices=CATEGORIA_VEICULO)
    cor = models.CharField(max_length=40, null=False, blank=False)
    ano_fabricacao = models.IntegerField(null=False, blank=False)

    class Meta:
        db_table = 'veiculo'

    def __str__(self):
        if self.categoria == 4:
            return format(self.placa + " - " + self.cor + " Ano " + self.ano_fabricacao)
        else:
            return format(self.placa + " - " + self.modelo + " Ano " + self.ano_fabricacao)

class Garagem(models.Model):
    id_pessoa = models.ForeignKey('Pessoa.pessoa', on_delete=models.RESTRICT, db_column='id_pessoa')
    ativa = models.BooleanField(default=True)

    class Meta:
        db_table = 'garagem'

    def __str__(self):
        return format(self.id_pessoa)

class GaragemVeiculo(models.Model):
    id_garagem = models.ForeignKey(Garagem, on_delete=models.RESTRICT, db_column='id_garagem')
    id_veiculo = models.ForeignKey(Veiculo, on_delete=models.RESTRICT, db_column='id_veiculo')
    
    class Meta:
        db_table = 'garagem_veiculo'

    def __str__(self):
        return format(self.id_veiculo)
        
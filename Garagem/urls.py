from django.urls import path
from .views import *

app_name = 'garagem'

urlpatterns = [
    path('veiculo/list/', veiculoListView),   
    path('garagemveiculo/', garagemVeiculoView),
    path('garagemveiculo/list', garagemVeiculoListView),
    path('garagemativa/list', garagemAtivaListView),
    path('pessoaveiculo/list', veiculoPessoaListView)
]
from rest_framework import serializers
from .models import *

class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veiculo
        fields = '__all__'

class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garagem
        fields = '__all__'

class GaragemVeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = GaragemVeiculo
        fields = '__all__'
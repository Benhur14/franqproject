from Pessoa.models import Pessoa
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *

@api_view(['GET'])
def veiculoListView(request):
    veiculos = Veiculo.objects.all().values()       
    return Response(veiculos)

@api_view(['POST', 'PUT'])
def garagemVeiculoView(request, *args, **kwargs):
    if request.method == 'POST' or request.method == 'PUT':
        id_veiculo =  request.data['id_veiculo']
        id_garagem =  request.data['id_garagem']        

        request.data.pop('id_veiculo')
        request.data.pop('id_garagem')

        garagem_veiculo = GaragemVeiculo(**request.data)
        garagem_veiculo.id_veiculo = Veiculo.objects.get(pk=id_veiculo)
        garagem_veiculo.id_garagem = Garagem.objects.get(pk=id_garagem)

        garagem_veiculo.save(request.user)
        return Response(garagem_veiculo.pk)

@api_view(['GET'])
def garagemVeiculoListView(request, *args, **kwargs):
    id_pessoa =  request.data['id_pessoa']        
    veiculos_garagem = GaragemVeiculo.objects.all().filter(id_garagem__id_pessoa=id_pessoa).extra(select={
        "veiculo": 
        " SELECT case categoria when 4 then concat('Placa: ', placa , ' Modelo: ', modelo, ' Ano: ', ano_fabricacao) "
        " else concat('Placa: ', placa, ' Cor: ', cor, ' Ano: ', ano_fabricacao) end "
        " FROM veiculo where veiculo.id = id_veiculo ",
    }).values('veiculo')

    return Response(veiculos_garagem)

@api_view(['GET'])
def garagemAtivaListView(request, *args, **kwargs):    
    garagens = Garagem.objects.all().filter(ativa=True).values("id")
    return Response(garagens)

@api_view(['GET'])
def veiculoPessoaListView(request, *args, **kwargs):       
    pessoas = Pessoa.objects.all().extra(select={
        "possui_veiculo":
        " select case when (select id from garagem_veiculo where garagem_veiculo.id_garagem = garagem.id limit 1) > 0 then 'True' else 'False' end "
        " from garagem where garagem.id_pessoa = pessoa.id ",        
    }).values('email','telefone','possui_veiculo')

    return Response(pessoas)